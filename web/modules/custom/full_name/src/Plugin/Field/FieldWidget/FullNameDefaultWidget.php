<?php
namespace Drupal\full_name\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
//use Drupal\Core\Field\WidgetBaseInterface;

/**
 * Plugin implementation of the 'FullNameDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "FullNameDefaultWidget",
 *   label = @Translation("Full Name Widget"),
 *   field_types = {
 *     "Full Name",
 *      "string"
 *   }
 * )
 */
class FullNameDefaultWidget extends WidgetBase {

  /**
   * Define the form for the field type.
   *
   * Inside this method we can define the form used to edit the field type.
   *
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    Array $element,
    Array &$form,
    FormStateInterface $formState
  ) {

    // First

    $element['first'] = [
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#default_value' => isset($items[$delta]->first) ?
        $items[$delta]->first : null,
      '#empty_value' => '',
      '#placeholder' => t('First Name'),
    ];

    // last

    $element['last'] = [
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#default_value' => isset($items[$delta]->last) ?
        $items[$delta]->last : null,
      '#empty_value' => '',
      '#placeholder' => t('Last Name'),
    ];


    return $element;
  }

} // class
