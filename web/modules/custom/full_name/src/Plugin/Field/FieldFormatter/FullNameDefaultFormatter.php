<?php
namespace Drupal\full_name\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal;

/**
 * Plugin implementation of the 'FullNameDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "FullNameDefaultFormatter",
 *   label = @Translation("Full Name"),
 *   field_types = {
 *     "FullName"
 *   }
 * )
 */
class FullNameDefaultFormatter extends FormatterBase {

  /**
   * Define how the field type is showed.
   *
   * Inside this method we can customize how the field is displayed inside
   * pages.
   */
  public function viewElements(FieldItemListInterface $items , $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => $item->first . ' ' . $item->last
      ];
    }

    return $elements;
  }

} // class
